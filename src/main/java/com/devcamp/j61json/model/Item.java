package com.devcamp.j61json.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

//Cách 4: sử dụng JsonIdentityInfo để chỉ ra load dữ liệu theo thuộc tính id
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Item {
    private int id;
	private String itemName;
    //@JsonIgnore//cách 2: sử dụng jsonignore đẻ bỏ qua phần generate json cho thuộc tính bị đệ quy
    //@JsonBackReference//Cách 3: bỏ qua thuộc tính của quan hệ phụ 	
	private User owner;
    
	public Item() {
    }

    public Item(int id, String itemName, User owner) {
		this.id = id;
		this.itemName = itemName;
		this.owner = owner;
	}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    //Cách 1: khong dung getter voi các thuoc tinh bi đệ quy
    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    
}
