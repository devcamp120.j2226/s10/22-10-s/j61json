package com.devcamp.j61json.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

//Cách 4: sử dụng JsonIdentityInfo để chỉ ra load dữ liệu theo thuộc tính id
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class User {
	private int id;
	private String name;
    
    //@JsonManagedReference//Cách 3: lấy thuộc tính của quan hệ chính 
	private List<Item> userItems;

	public User() {
    }
    public User(int id, String name) {
		this.id = id;
		this.name = name;
		userItems = new ArrayList<>();
	}
	public void addItem(Item item) {
		this.userItems.add(item);
	}
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public List<Item> getUserItems() {
        return userItems;
    }
    public void setUserItems(List<Item> userItems) {
        this.userItems = userItems;
    }
    
}
